package ForkJoin;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;

public class ProcesadorArchivosTest {

	public static void main(String[] args) {

		List<String> archivosDescargaFiltrados = new ArrayList<>();
		List<String> archivosDocumentosFiltrados = new ArrayList<>();

		ForkJoinPool pool = new ForkJoinPool(4);

		ProcesadorArchivos descargas = new ProcesadorArchivos("/", "txt");
		//ProcesadorArchivos documentos = new ProcesadorArchivos("/home/dflamprea/Documents", "class");

		pool.execute(descargas);
		//pool.execute(documentos);

		do {
			try {
				System.out.println("Paralismo:" + pool.getParallelism());
				System.out.println("Hilos Activos:" + pool.getActiveThreadCount());
				System.out.println("Encolados:" + pool.getQueuedTaskCount());
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} while ((!descargas.isDone()));

		pool.shutdown();
		archivosDescargaFiltrados = descargas.join();
		//archivosDocumentosFiltrados = documentos.join();
		System.out.println("Descargas hay: " + archivosDescargaFiltrados.size());
		System.out.println("Documentos hay: " + archivosDocumentosFiltrados.size());

	}

}
