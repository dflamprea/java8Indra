package ForkJoin;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

public class ProcesadorArchivos extends RecursiveTask<List<String>> {

	private final String path;
	private final String extension;

	public ProcesadorArchivos(String path, String extension) {
		this.path = path;
		this.extension = extension;
	}

	@Override
	protected List<String> compute() {
		
		List<String> archivosEncontrados = new ArrayList<String>();
		List<ProcesadorArchivos> listaTareas = new ArrayList<ProcesadorArchivos>();
		
		File archivo = new File(this.path);
		File contenido[] = archivo.listFiles();
		
		if(contenido != null) {
			
			for(int i=0; i < contenido.length; i++) {
				if (contenido[i].isDirectory()) {
					ProcesadorArchivos tarea = new ProcesadorArchivos(contenido[i].getAbsolutePath(), extension);
					tarea.fork();
					listaTareas.add(tarea);
					
					
				} else {
					if (contenido[i].getName().endsWith(extension)) {
						//System.out.println(contenido[i].getAbsolutePath());
						archivosEncontrados.add(contenido[i].getAbsolutePath());
					}
				}
			}
			
		}
		
		agregaResultados(archivosEncontrados, listaTareas);
		
		return archivosEncontrados;
	}
	 	 		
	
	private void agregaResultados(List<String> archivosEncontrados, List<ProcesadorArchivos> listaTareas) {
		
		for (ProcesadorArchivos hilo: listaTareas) {
			archivosEncontrados.addAll(hilo.join());
		}
		
	}

}
