package fechas;

import static java.time.DayOfWeek.*;
import static java.time.temporal.ChronoUnit.*;
import static java.time.temporal.TemporalAdjusters.*;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class FechaHora {

	public static void main(String[] args) {
		LocalDate now, bDate;

		now = LocalDate.now();
		// new Date()
		System.out.println("Fecha actual: " + now);

		bDate = LocalDate.of(2023, 7, 4);
		System.out.println("Fecha de nacimiento: " + bDate);

		// Saber si la fecha es anterior (pasada)
		System.out.println(bDate.isBefore(now));

		// Saber el dia de la semana
		System.out.println(bDate.getDayOfWeek());

		// Saber si el año es bisiesto
		System.out.println(bDate.isLeapYear());

		// Agregar un mes
		System.out.println(now.plusWeeks(3));

		System.out.println(now.with(next(SUNDAY)));

		// TIME
		LocalTime nowTime;
		nowTime = LocalTime.now();
		System.out.println("Hora actual: " + nowTime);

		System.out.println(nowTime.plusHours(2).plusMinutes(10));

		System.out.println(nowTime.truncatedTo(HOURS));

		System.out.println(nowTime.toSecondOfDay() / 60);

		LocalTime finClase = LocalTime.of(21, 30);
		System.out.println(LocalTime.of(21, 45));

		System.out.println(nowTime.until(LocalTime.of(21, 45), MINUTES));

		LocalDateTime nowDateTime;
		nowDateTime = LocalDateTime.now();
		System.out.println("Fecha y Hora actual: " + nowDateTime);

		System.out.println(LocalDateTime.of(1992, Month.DECEMBER, 24, 23, 48));

		System.out.println(LocalDateTime.of(bDate, finClase));

		System.out.println(nowDateTime.plusDays(3).plusHours(4).plusMinutes(5));

		// Zones
		ZoneId LONDON = ZoneId.of("Europe/London");
		ZoneOffset.of("-5");

		// ZoneOffset.ofHoursMinutes(-4, 30);

		ZonedDateTime dateTimeLondon = ZonedDateTime.of(now, nowTime, LONDON);
		ZonedDateTime currentDateTimeLondon = ZonedDateTime.now(LONDON);

		System.out.println(dateTimeLondon);
		System.out.println(currentDateTimeLondon);

		ZoneId USEste = ZoneId.of("America/New_York");
		LocalDate cambioHora = LocalDate.of(2023, Month.MARCH, 11);
		LocalTime dayly = LocalTime.of(10, 00);

		System.out.println(ZonedDateTime.of(cambioHora, dayly, USEste));
		System.out.println(ZonedDateTime.of(cambioHora, dayly, USEste).plusDays(1));

		// Instant entrega la hora en GMT
		try {

			Instant ahora = Instant.now();
			Thread.sleep(1000);
			Instant despues = Instant.now();

			System.out.println(ahora);
			System.out.println(despues);

			// Period: Obtiene la diferencia entre dos fechas
			Period period = Period.between(now, bDate);
			System.out.println(
					"Años: " + period.getYears() + " Meses: " + period.getMonths() + " Dias: " + period.getDays());

			// Duration
			Duration duration = Duration.between(nowTime, finClase);
			System.out.println("Minutos:" + duration.toMinutes());

			String fecha = "2022/09/10 18:54";
			DateTimeFormatter formatterUSA = DateTimeFormatter.ofPattern("yyyy/dd/MM HH:mm");
			DateTimeFormatter formatterCOL = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
			LocalDateTime ldt = LocalDateTime.parse(fecha, formatterUSA);
			System.out.println(ldt.format(formatterCOL));

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");

			System.out.println("Fecha con java.Date: " + sdf.parse("2022/09/10 18:54"));
			System.out.println("Fecha con java.Date: " + sdf.format(sdf.parse("2022/09/10 18:54")));

			Date today = new Date();
			ldt = LocalDateTime.ofInstant(today.toInstant(), ZoneId.systemDefault());
			System.out.println("--" + ldt.format(formatterCOL));

			ZonedDateTime zdt = ldt.atZone(ZoneId.systemDefault());
			System.out.println(Date.from(zdt.toInstant()));

			System.out.println("Diferencia en dias: " + DAYS.between(now, bDate));

			// Practica 9
			LocalDate fechaNacimiento = LocalDate.of(1949, 7, 4);
			LocalDate fechaMuerte = LocalDate.of(2001, 3, 3);

			System.out.println("Tenia :" + fechaNacimiento.until(fechaMuerte, YEARS));
			System.out.println("Tenia Dias:" + fechaNacimiento.until(fechaMuerte, DAYS));
			System.out.println("Dia del año:" + fechaNacimiento.getDayOfYear());
			System.out.println("Dia semana muerte:" + fechaMuerte.getDayOfWeek());
			System.out.println("es bisiesto:" + fechaNacimiento.isLeapYear());
			System.out.println("Tenia Decadas:" + fechaNacimiento.until(fechaMuerte, DECADES));

			System.out.println("Dia semana cuando 30:" + fechaNacimiento.plusYears(30).getDayOfWeek());

		} catch (Exception e) {
			// TODO: handle exception
		}

		

	}

}
