package fechas;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Stream;

public class NIO {
	
	public static void main(String[] args) {
		// NIO.2

				FileSystem fs = FileSystems.getDefault();
				Path path1 = fs.getPath("/home/dflamprea/Documents/../entrenamiento/angularITC/ProyectoAngular/package.jso");
				Path path2 = fs.getPath("/home", "dflamprea", "la divina comedia.txt");

				System.out.println("Nombre del archivo:" + path1.getFileName());
				System.out.println("Padre:" + path1.getParent());
				System.out.println("NameCount:" + path1.getNameCount());
				System.out.println("Root:" + path1.getRoot());
				System.out.println("Es Absuluta:" + path1.isAbsolute());
				System.out.println("URI:" + path1.toUri());
				System.out.println("Normalizado:" + path1.normalize());

				Path path3 = path1.subpath(2, 4);
				System.out.println(path3.getFileName());

				Path path4 = Paths.get("/home/dflamprea");
				Path path5 = path4.resolve("Downloads/ejemplo.txt");

				System.out.println("URI:" + path5.toUri());

				System.out.println("Existe el path5 " + Files.exists(path5));

				try {
					Files.deleteIfExists(path5);

					if (Files.notExists(path5)) {

						Files.createFile(path5);

					}

					Path path6 = path4.resolve("Downloads/Practica Java 8");

					if (Files.notExists(path6)) {
						Files.createDirectories(path6);

					}
					
					Path path7 = path6.resolve("nuevoArchivo.txt");
					Path path8 = path6.resolve("nuevoArchivo2.txt");

					Files.copy(path5, path7, StandardCopyOption.REPLACE_EXISTING);
					Files.move(path5, path8, StandardCopyOption.REPLACE_EXISTING);
					
					
					//Listar archivos de un directorio
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				Path path66 = path4.resolve("Downloads/Practica Java 8");
				System.out.println("Listar directorio");
				//Listar elementos de un directorio
				try (Stream<Path> files = Files.list(path66)) {
					
					files.forEach(archivo -> System.out.println(archivo));
					
				} catch (IOException e) {
					// TODO: handle exception
				}
				
				//Verificar la estructura de un directorio
				
				System.out.println("Estructura del directorio");
				
				try (Stream<Path> files = Files.walk(path66)) {
					
					files.forEach(archivo -> System.out.println(archivo));
					
				} catch (IOException e) {
					// TODO: handle exception
				}
				
				
				System.out.println("Leer contenido de un archivo");
				
				try (BufferedReader bReader = new BufferedReader(new FileReader("/home/dflamprea/Downloads/Practica Java 8/nombres.txt"))) {
					
					bReader.lines().forEach(line -> System.out.println(line));
					
				} catch (IOException e) {
					// TODO: handle exception
				}
				
				
				System.out.println("Leer contenido de un archivo y filtrarlo");
				
				try (BufferedReader bReader = new BufferedReader(new FileReader("/home/dflamprea/Downloads/Practica Java 8/nombres.txt"))) {
					
					bReader.lines()
					.filter(line -> line.contains("Liseth"))
					.forEach(line -> System.out.println(line));
					
				} catch (IOException e) {
					// TODO: handle exception
				}
				
				
				
				System.out.println("Convertir en una lista");
				
				try {
					
					List<String> array = Files.readAllLines(Paths.get("/home/dflamprea/Downloads/Practica Java 8/nombres.txt"));
					
					array.stream().filter(line -> line.contains("Liseth"))
					.forEach(line -> System.out.println(line));
					
					
				} catch (IOException e) {
					// TODO: handle exception
				}
				
				
				try {
					System.out.println(Files.size(Paths.get("/home/dflamprea/Downloads/Practica Java 8/nombres.txt")));
					System.out.println(Files.isDirectory(Paths.get("/home/dflamprea/Downloads/Practica Java 8/nombres.txt")));
					System.out.println(Files.isRegularFile(Paths.get("/home/dflamprea/Downloads/Practica Java 8/nombres.txt")));
					System.out.println(Files.isHidden(Paths.get("/home/dflamprea/Downloads/Practica Java 8/.nombres2.txt")));
					System.out.println(Files.getLastModifiedTime(Paths.get("/home/dflamprea/Downloads/Practica Java 8/nombres.txt")));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				
				Path path7 = Paths.get("/home/dflamprea/Downloads/Practica Java 8/nombresCopia.txt");
				
				try (BufferedReader reader = new BufferedReader(new FileReader("/home/dflamprea/Downloads/Practica Java 8/nombres.txt"))) {
					
					reader.lines()
					.forEach(line -> {
						
						try {
							Files.write(path7, (line + System.lineSeparator()).getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					});
					
					
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				
				
	}

}
