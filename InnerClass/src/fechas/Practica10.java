package fechas;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class Practica10 {
	
	public static void main(String[] args) {
		
		/* 1
		Path path1 = Paths.get("/home/dflamprea/Downloads/la divina comedia.txt");
		Path path2 = Paths.get("/home/dflamprea/Downloads/la divina comedia2.txt");
		try (BufferedReader bReader = new BufferedReader(new FileReader(path1.toString()))) {
			
			bReader.lines()
			//.filter(line -> line.contains("Liseth"))
			.forEach(line -> { 
				try {
					System.out.println(line);
					Files.write(path2, (line + System.lineSeparator()).getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			
		} catch (IOException e) {
			// TODO: handle exception
		}*/
		
		
		
		/* 2
		Path path1 = Paths.get("/home/dflamprea/Downloads/la divina comedia.txt");
		Path path2 = Paths.get("/home/dflamprea/Downloads/la divina comedia3.txt");
		try (BufferedReader bReader = new BufferedReader(new FileReader(path1.toString()))) {
			
			bReader.lines()
			.filter(line -> line.startsWith("Dante:"))
			.forEach(line -> { 
				try {
					System.out.println(line);
					Files.write(path2, (line + System.lineSeparator()).getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			
		} catch (IOException e) {
			// TODO: handle exception
		}
		*/
		
		
		/* 4
		Path path1 = Paths.get("/home/dflamprea/Downloads/la divina comedia.txt");
		Path path2 = Paths.get("/home/dflamprea/Downloads/la divina comedia4.txt");
		
		try {
			List<String> array = Files.readAllLines(path1);
			array.stream().map(line -> line.toUpperCase())
			.forEach(line -> {
				try {
					System.out.println(line);
					Files.write(path2, (line + System.lineSeparator()).getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		
		/*Path path1 = Paths.get("/home/dflamprea/Downloads/la divina comedia.txt");
		Path path2 = Paths.get("/home/dflamprea/Downloads/la divina comedia4.txt");
		
		try {
			List<String> array = Files.readAllLines(path1);
			array.stream()
			.filter(line -> line.contains("donde"))
			.map(line-> {
				
				line.split(" ");
				return "";
			})
			.forEach(line -> {
				try {
					System.out.println(line);
					Files.write(path2, (line + System.lineSeparator()).getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		
		
		
		
		
		
	}

}
