package dbConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	
	private static DBConnection instance = null;
	
	
	private Connection conn;
	private static final String URL = "jdbc:mysql://15.235.45.76:3306/training";
	private static final String USUARIO = "training";
	private static final String CONTRASENA = "6DMgzfu0vvx02xOxaK1W";
	
	
	private DBConnection() throws SQLException {
		conn = DriverManager.getConnection(URL, USUARIO, CONTRASENA);
	}


	public Connection getConn() {
		return conn;
	}
	
	public static DBConnection getInstance() throws SQLException {
		if (instance == null) {
			instance = new DBConnection();
		} else if (instance.getConn().isClosed()){
			instance = new DBConnection();
		}
		return instance;
	}
	
	
    
    
	
	/*public static void main(String[] args) {
    	
    	try (Connection conn = DriverManager.getConnection(URL, USUARIO, CONTRASENA);
    			Statement stmt = conn.createStatement();
    			ResultSet rs = stmt.executeQuery("SELECT * FROM PERSONA")) {
    		
    		while (rs.next()) {
    			
    			System.out.println("ID de la persona:" + rs.getInt("personId"));
    			System.out.println("Nombre de la persona:" + rs.getString(2));
    			System.out.println("Apellido de la persona:" + rs.getString("lastName"));
    			System.out.println("Direccion de la persona:" + rs.getString("address"));
    			System.out.println("Ciudad de la persona:" + rs.getInt("city"));
    			System.out.println("Tipo de identificacion de la persona:" + rs.getString("tipoIdentificacion"));
    			System.out.println("Numero de identificacion de la persona:" + rs.getString("numeroIdentificacion"));
    			
    			
    		}
    		
    		
    	} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}*/

}
