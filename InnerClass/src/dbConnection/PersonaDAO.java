package dbConnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dto.PersonaDTO;

public class PersonaDAO {


	public List<PersonaDTO> findAll() throws SQLException {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		List<PersonaDTO> personas = new ArrayList<>();

		try {

			conn = DBConnection.getInstance().getConn();
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM PERSONA");

			while (rs.next()) {
				personas.add(new PersonaDTO(Long.valueOf(rs.getInt("personId")), rs.getString(2), rs.getString(3),
						rs.getString("address"), Long.valueOf(rs.getInt("city")), rs.getString("tipoIdentificacion"),
						rs.getString("numeroIdentificacion")));
			}
			
			return personas;

		} catch (SQLException e) {
			e.printStackTrace();
			throw(e);

		} finally {

			try {

				if (rs != null) {
					rs.close();
				}

				if (stmt != null) {
					stmt.close();
				}

				if (conn != null) {
					conn.close();
				}
				
			} catch (final SQLException e) {
				e.printStackTrace();
				throw(e);
			}
			
			
		}

	}

}
