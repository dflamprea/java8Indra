package interfaces;

public class BotonCrear implements IButton{

	@Override
	public String clickIzquierdo() {
		return "Invoca el servicio de crear Persona";
	}

	@Override
	public String clickDerecho() {
		return "Muestarpopup con la info de la persona";
	}

}
