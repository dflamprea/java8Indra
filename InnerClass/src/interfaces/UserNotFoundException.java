package interfaces;

public class UserNotFoundException extends Exception {
	
	private static final long serialVersionUID = -7083866142833140228L;

	public UserNotFoundException() {

	}
	
	
	public UserNotFoundException(String message ) {
		super(message);
	}
	
	public UserNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
