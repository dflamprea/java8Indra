package interfaces;

import java.util.HashMap;

public class Practica8 {
	
	private HashMap<String, String> usuarios;
	
	public Practica8() {
		usuarios = new HashMap<String, String>();
		usuarios.put("lpjimenez", "123456");
		usuarios.put("jrvelosa", "000001");
		usuarios.put("japerez", "999999");
		usuarios.put("jsfresneda", "contraseña");
		usuarios.put("afescobar", "onceunos");
	}
	
	
	public Boolean login(String usuario, String contraseña) throws UserNotFoundException, InvalidPasswordException {
		
		
		if (!usuarios.containsKey(usuario)) {
			
			throw new UserNotFoundException(String.format("El usuario %s no existe", usuario));
		
		} else {
		
			if (contraseña.equals(usuarios.get(usuario))) {
				return true;
			
			} else {
				throw new InvalidPasswordException("Contraseña invalida");
			}
		}

	}
	
	
	

}
