package interfaces;

public class SumatoriaMain {

	public static void main(String[] args) {
        int a[]={1,2,3,4,5, 6};
        SumatoriaHilo mh1 = SumatoriaHilo.creaEInicia("#1",a);
        SumatoriaHilo mh2 = SumatoriaHilo.creaEInicia("#2",a);
        SumatoriaHilo mh3 = SumatoriaHilo.creaEInicia("#3",a);
        try {
            mh1.hilo.join();
            mh2.hilo.join();
            mh3.hilo.join();
        }catch (InterruptedException exc){
            System.out.println("Hilo principal interrumpido.");
        }
    }
	
}
