package interfaces;

public class Maximo<T> {
	
	public <T extends Comparable<T>>T max(T v1, T v2, T v3) {
		
		T mx = v1;
		
		if (v2.compareTo(mx) > 0) {
			mx = v2;
		}
		
		if (v3.compareTo(mx) > 0) {
			mx = v3;
		}
		
		return mx;
		
	}

}
