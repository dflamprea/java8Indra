package interfaces;

public class SumatoriaHilo implements Runnable {
	
	Thread hilo;
    static Sumatoria sumarray= new Sumatoria();
    int a[];
    int resp;
    //Construye un nuevo hilo.
    SumatoriaHilo(String nombre, int nums[]){
        hilo= new Thread(this,nombre);
        a=nums;
    }
    //Un método que crea e inicia un hilo
    public static SumatoriaHilo creaEInicia (String nombre,int nums[]){
    	SumatoriaHilo miHilo=new SumatoriaHilo(nombre,nums);
        miHilo.hilo.start(); //Inicia el hilo
        return miHilo;
    }
    //Punto de entrada del hilo
    public void run(){
        int sum;
        System.out.println(hilo.getName()+ " iniciando.");
        //synchronize llama a sumArray()
        synchronized (sumarray) {
        	resp=sumarray.sumArray(a);
		}
        
        System.out.println("Suma para "+hilo.getName()+ " es "+resp);
        System.out.println(hilo.getName()+ " terminado.");
    }

}
