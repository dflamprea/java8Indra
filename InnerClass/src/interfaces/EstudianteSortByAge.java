package interfaces;

import java.util.Comparator;

public class EstudianteSortByAge implements Comparator<Estudiante> {

	@Override
	public int compare(Estudiante o1, Estudiante o2) {
		int resultado = o1.getEdad().compareTo(o2.getEdad());
		
		if (resultado != 0) {
			return resultado;
		} else {
			return o1.getApellido().compareTo(o2.getApellido());
		}
	}

}
