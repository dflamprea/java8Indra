package interfaces;

public class PrimeNumberException extends Exception {
	
	private static final long serialVersionUID = -7083866142833140228L;

	public PrimeNumberException() {

	}
	
	
	public PrimeNumberException(String message ) {
		super(message);
	}
	
	public PrimeNumberException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
