package interfaces;

public interface IButton {
	
	public String clickIzquierdo();
	
    public String clickDerecho();

}
