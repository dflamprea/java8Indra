package interfaces;

public class InvalidPasswordException extends Exception {
	
	private static final long serialVersionUID = -7083866142833140228L;

	public InvalidPasswordException() {

	}
	
	
	public InvalidPasswordException(String message ) {
		super(message);
	}
	
	public InvalidPasswordException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
