package interfaces;

import java.util.List;

public class Estudiante implements Comparable<Estudiante> {
	
	
	private String nombre;
	private String apellido;
	private String genero;
	private Long edad;
	private String ciudad;
	private List<Double> notas;
	
	
	public Estudiante(String nombre, String apellido, String genero, Long edad, String ciudad) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.genero = genero;
		this.edad = edad;
		this.ciudad = ciudad;
		this.notas = notas;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellido() {
		return apellido;
	}


	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public String getGenero() {
		return genero;
	}


	public void setGenero(String genero) {
		this.genero = genero;
	}


	public Long getEdad() {
		return edad;
	}


	public void setEdad(Long edad) {
		this.edad = edad;
	}


	public String getCiudad() {
		return ciudad;
	}


	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}


	public List<Double> getNotas() {
		return notas;
	}


	public void setNotas(List<Double> notas) {
		this.notas = notas;
	}


	@Override
	public int compareTo(Estudiante o) {
		int resultado = this.apellido.compareTo(o.getApellido());
		
		if (resultado > 0 ) {
			return 1;
		} else if (resultado < 0) {
			return -1;
		} else {
			return 0;
		}
	}
	
	

}
