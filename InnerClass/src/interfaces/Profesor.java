package interfaces;

public class Profesor {
	
	private final String nombre;
	private final String apellido;
	private final Long edad;
	private final String direccion;
	private final Double salario;
	private final String ciudad;
	
	public Profesor(Builder builder) {
		this.nombre = builder.nombre;
		this.apellido = builder.apellido;
		this.edad = builder.edad;
		this.direccion = builder.direccion;
		this.salario = builder.salario;
		this.ciudad = builder.ciudad;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public String getApellido() {
		return apellido;
	}
	
	public Long getEdad() {
		return edad;
	}
	
	public String getDireccion() {
		return direccion;
	}
	
	
	
	public Double getSalario() {
		return salario;
	}

	

	public String getCiudad() {
		return ciudad;
	}

	
	@Override
	public String toString() {
		return "Profesor [nombre=" + nombre + ", apellido=" + apellido + ", edad=" + edad + ", direccion=" + direccion
				+ ", salario=" + salario + ", ciudad=" + ciudad + "]";
	}

	public static class Builder {
		
		private final String nombre;
		private final String apellido;
		private Long edad;
		private String direccion;
		private Double salario;
		private String ciudad;
		
		public Builder(String nombre, String apellido) {
			this.nombre = nombre;
			this.apellido = apellido;
		}
		
		public Builder edad(Long edad) {
			this.edad = edad;
			return this;
		}
		
		public Builder direccion(String direccion) {
			this.direccion = direccion;
			return this;
		}
		

		public Builder salario(Double salario) {
			this.salario = salario;
			return this;
		}
		
		public Builder ciudad(String ciudad) {
			this.ciudad = ciudad;
			return this;
		}
		
		
		public Profesor build() {
			return new Profesor(this);
		}
		
	}
	
	

}
