package interfaces;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.DoubleFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToDoubleFunction;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

import javax.print.DocFlavor.STRING;

public class Main {

	public static void main(String[] args) {
		
		
		IButton b1 = new IButton() {
			
			@Override
			public String clickIzquierdo() {
				return "Invoca el servicio de crear Persona";
			}
			
			@Override
			public String clickDerecho() {
				return "Muestarpopup con la info de la persona";
			}
		};
		
		
		IButton b2 = new IButton() {
			
			@Override
			public String clickIzquierdo() {
				return "Invoca el servicio de elimianar Persona";
			}
			
			@Override
			public String clickDerecho() {
				return null;
			}
		};
		
		System.out.println(b1.clickIzquierdo());
		System.out.println(b2.clickIzquierdo());
		
		
		
		
		/*IStringUtils su = new IStringUtils() {
			
			@Override
			public String transformar(String s) {
				// TODO Auto-generated method stub
				return s.toUpperCase();
			}
		};*/
		
	/*	IStringUtils su2 = s -> { if (s.length()> 2 ) {
		                          	return s.toUpperCase();
								  } else {
									  return s;
								  }
		                        };*/
		
		
		
//		System.out.println(su.transformar("diego lamprea"));
///		System.out.println(su2.transformar("d"));
		
		
		
		
		Estudiante e1 = new Estudiante("Jhon", "Duque", "M", 30L, "Cali");
		Estudiante e4 = new Estudiante("Juana", "Diaz", "M", 30L, "Cali");
		Estudiante e2 = new Estudiante("Jakeline", "Valencia", "F", 28L, "Medellin");
		Estudiante e3 = new Estudiante("Yeray", "Castañeda", "M", 36L, "Bogota");
		
		Set<Estudiante> estudiantesSet = new TreeSet<>();
		
		estudiantesSet.add(e1);
		estudiantesSet.add(e2);
		estudiantesSet.add(e3);
		
		System.out.println("ordenamiento con Comparable-------------");
		
		for (Estudiante e: estudiantesSet) {
			System.out.println("Nombre: " + e.getNombre() + " Apellidos:" + e.getApellido() + " Edad: " + e.getEdad()  );
		}
		
		
		
		System.out.println("ordenamiento con Comparator-------------");
		List<Estudiante> estudiantesList = new ArrayList<>();
		
		estudiantesList.add(e1);
		estudiantesList.add(e2);
		estudiantesList.add(e3);
		estudiantesList.add(e4);
		
		Comparator<Estudiante> sortByAge = new EstudianteSortByAge();
		
		Collections.sort(estudiantesList, sortByAge);
		for (Estudiante e: estudiantesList) {
			System.out.println("Nombre: " + e.getNombre() + " Apellidos:" + e.getApellido() + " Edad: " + e.getEdad()  );
		}
		
		
		List ejemplo = new ArrayList<>();
		ejemplo.add("sssss");
		ejemplo.add(1);
		ejemplo.add(1.0);
		
		for (Estudiante e: estudiantesSet) {
			
			IStringUtils mayusculas = (n, a, c, g) -> new StringBuilder(n.toUpperCase()).append(" ").append(a.toUpperCase()).toString();
			IStringUtils contador = (n, a, c, g) -> String.valueOf(n.length() + a.length());
			IStringUtils isMedellin = (n, a, c, g) -> String.valueOf(c.equals("Medellin"));
			
			
			IStringUtils isFemale = new IStringUtils() {
				
				@Override
				public String transformar(String s1, String s2, String s3, String s4) {
					
					return String.valueOf(s4.equals("F"));
				}
			};
			
			
			System.out.println("A Y B: " + mayusculas.transformar(e.getNombre(), e.getApellido(), null, null));
			System.out.println("C: " + contador.transformar(e.getNombre(), e.getApellido(), null, null));
			System.out.println("D: " + isMedellin.transformar(null, null, e.getCiudad(), null));
			System.out.println("E: " + isFemale.transformar(null, null, null, e.getGenero()));
		
		}
		
		
		
		
		Cache<Estudiante> cacheEstudiantes = new Cache<Estudiante>();
		
/*		Cache<Profesor> cacheProfesor = new Cache<>();
		cacheProfesor.add(new Profesor());
		
		
		CacheEstudiante cacheE = new CacheEstudiante();
		cacheE.add(e1);
		cacheE.add(new Profesor());*/
		
		
		
		Integer[] intArray = {1,2,3,4};
		Double[] doubleArray = {1.0,2.0,3.0,4.0};
		String[] stringArray = {"1.0","2.0","3.0","4.0"};
		
		
		print(intArray);
		print(doubleArray);
		print(stringArray);
		
		
		Maximo<Long> ml = new Maximo<>();
		System.out.println(ml.max(4, 2, 3));
		
		Maximo<Double> md = new Maximo<>();
		System.out.println(md.max(5.4, 10.0, 8.2));
		
		Maximo<String> ms = new Maximo<>();
		System.out.println(md.max("w","r", "f"));
		
		
		
		
		List<String> list = new ArrayList<>();
		list.add("uno");
		list.add("dos");
		list.add("tres");
		list.add("tres");
		
		for (String l : list) {
			System.out.println(l);
		}
		
		System.out.println("SET---------------------");
		
		
		Set<String> set = new TreeSet<>();
		set.add("uno");
		set.add("dos");
		set.add("tres");
		set.add("tres");
		
		for (String s : set) {
			System.out.println(s);
		}
		
		//No se respeta el orden
		Hashtable<String , String> htMap = new Hashtable<>();
		//htMap.put(null, "Seis");
		htMap.put("3", "Tres");
		htMap.put("9", "Nueve");
		htMap.put("1", "Uno");
		htMap.put("12", "Doce");
		
		Iterator it2 = htMap.entrySet().iterator();
		while (it2.hasNext()) {
			
			Map.Entry<String, String> e = (Map.Entry<String, String>)it2.next();
			
			System.out.println(e.getKey() + " " + e.getValue());
			
		}
		
		System.out.println("HashMap---------------------");
		
		
		//No se respeta el orden
		HashMap<String , String> hsMap = new HashMap<>();
		hsMap.put(null, "Seis");
		hsMap.put("3", "Tres");
		hsMap.put("9", "Nueve");
		hsMap.put("1", "Uno");
		hsMap.put("12", "Doce");
		
		Iterator it = hsMap.entrySet().iterator();
		while (it.hasNext()) {
			
			Map.Entry<String, String> e = (Map.Entry<String, String>)it.next();
			
			System.out.println(e.getKey() + " " + e.getValue());
			
		}
		
		System.out.println(hsMap.get("9"));
		
		System.out.println("TreeMap---------------------");
		//se respeta el orden a partir de la llave
		TreeMap<String , String> tMap = new TreeMap<>();
		tMap.put("6", "Seis");
		tMap.put("3", "Tres");
		tMap.put("9", "Nueve");
		tMap.put("1", "Uno");
		tMap.put("12", "Doce");
		
		Iterator itt = tMap.entrySet().iterator();
		while (itt.hasNext()) {
			
			Map.Entry<String, String> e = (Map.Entry<String, String>)itt.next();
			
			System.out.println(e.getKey() + " " + e.getValue());
			
		}
		
		
		
		
		System.out.println("LinkedHashMap---------------------");
		//se respeta el orden en que van ingresando
		LinkedHashMap<String , String> lhMap = new LinkedHashMap<>();
		lhMap.put("6", "Seis");
		lhMap.put("3", "Tres");
		lhMap.put("9", "Nueve");
		lhMap.put("1", "Uno");
		lhMap.put("12", "Doce");
		
		Iterator itlhm = lhMap.entrySet().iterator();
		while (itlhm.hasNext()) {
			
			Map.Entry<String, String> e = (Map.Entry<String, String>)itlhm.next();
			
			System.out.println(e.getKey() + " " + e.getValue());
			
		}
		
		System.out.println("Deque---------------------");
		Deque<String> dq = new LinkedList<>();
		//Agrega un elemento al final
		dq.add("uno");
		dq.add("dos");
		dq.add("tres");
		
		
		//Agrega un elemento al inicio
		dq.addFirst("cero");
		//Agrega un elemento al final
		dq.addLast("cuatro");

		//Agrega un elemento al inicio
		dq.push("-uno");
		dq.offer("cinco");
		
		dq.getFirst();
		dq.removeFirst();
		dq.removeLast();
		
		
		
		for (String s : dq) {
			System.out.println(s);
		}
		
		
		
		
		
		List<Profesor> profesorList = createPofesor();
		
		
		for (int i=0; i < profesorList.size(); i++) {
			System.out.println(profesorList.get(i));
		}
		
		for (Profesor p : profesorList) {
			if (p.getEdad() >= 40) {
				System.out.println(p);
			}
		}
		
		System.out.println("FOR EACH------------------");
		
		profesorList.forEach(p -> System.out.println(p));
		
		
		Predicate<Profesor> veteranos = p -> p.getEdad() >= 40L;
		Predicate<Profesor> iniciePorC = p -> p.getNombre().startsWith("C");
		
		
		System.out.println("FILTRO MAYORES DE 40------------");
		profesorList.stream()
			.parallel()
		 	.filter(iniciePorC)
		 	.filter(veteranos)
		 	.mapToLong(Profesor::getEdad)
		 	.average();
			//.forEach(p -> System.out.println(p));
		
		
		
		
		System.out.println("PREDICADO INGRESA UN OBJETO Y RETORNA UN BOOLEAN------------");
		Predicate<Profesor> edadProfesores = p -> p.getEdad() > 30;
		profesorList.stream()
	 	.filter(edadProfesores)
	 	.forEach(p -> System.out.println(p));
	
		
		System.out.println("CONSUMER INGRESA UN OBJETO Y NO RETORNA NADA------------");	
		Consumer<Profesor> nombresProfesor = p -> System.out.println(p);;
		profesorList.stream().forEach(nombresProfesor);
		
		System.out.println("FUNCTION INGRESA UN OBJETO Y RETORNA OTRO------------");
		Function<Profesor, String> nameToUpperCase = p -> p.getNombre().toUpperCase();
		//profesorList.stream().
		
		System.out.println(nameToUpperCase.apply(profesorList.get(0)));
		
		
		System.out.println("SUPPLIER NO INGRESA NADA Y RETORNA UN OBJETO------------");
		
		Supplier<Profesor> profesorSupplier = () -> new Profesor.Builder("Juan", "Castañeda")
				.salario(3500000.0)
				.ciudad("Cali")
				.edad(25L)
				.build();
		
		profesorList.add(profesorSupplier.get());
		profesorList.stream()
	 	.forEach(p -> System.out.println(p));
		
		System.out.println("ToDoubleFunction INGRERSA UN OBJETO Y RETORNA UN PRIMITIVO------------");
		
		ToDoubleFunction<Profesor> bonificacionProfesor = p -> p.getSalario() * 1.1;
		profesorList.forEach(p-> System.out.println(bonificacionProfesor.applyAsDouble(p)));

		System.out.println("DoubleFunction INGRERSA UN PRIMITIVO Y RETORNA UN OBJETO ------------");
		
		DoubleFunction<String> calculo = t -> String.valueOf(t * 10);
		System.out.println(calculo.apply(100));
		
		
		System.out.println("BiPredicado INGRERSAN DOS OBJETOS Y RETORNA UN BOOLEAN ------------");
		
		BiPredicate<Profesor, String> profesorStartWith = (p, s) -> p.getNombre().startsWith(s); 
		System.out.println(profesorStartWith.test(profesorList.get(2), "N"));
		
		
		System.out.println("UNARIO INGRERSAN Y RETORNA EL MISMO TIPO DE DATO ------------");
		UnaryOperator<String> unary = s -> s.toUpperCase();
		System.out.println(unary.apply("Practica con operador unario"));
		
		
		System.out.println("OPERACIONES CON LAMBDA ------------");
		System.out.println("OPERACION MAP ------------");
		profesorList.stream()
		.filter(p -> p.getSalario() >  2000000.0)
		.map(p -> p.getSalario() * 0.11)
		.forEach( r -> System.out.println("Retencion de:" + r));
		
		System.out.println("OPERACION PEEK ------------");
		profesorList.stream()
		.filter(p -> p.getCiudad().equals("Cali"))
		//.filter(p -> p.getEdad() > 40)
		.filter(p -> p.getSalario() != null && p.getSalario() >=  2000000.0)
		.peek(p -> System.out.println(p))
		.map(p -> p.getSalario() * 0.11)
		.forEach( r -> System.out.println("Retencion de:" + r));
		
		
		System.out.println("OPERACION FindFisrt ------------");
		Optional<Profesor> oProfesor = profesorList.stream()
		.filter(p -> p.getCiudad().equals("Medellin"))
		.findFirst();
		
		if (oProfesor.isPresent()) {
			System.out.println(oProfesor.get());
		}
		
	 	
		System.out.println("OPERACION AnyMatch ------------");
		if (profesorList.stream().anyMatch(p -> p.getCiudad().equals("Bogota"))) {
		
			oProfesor = profesorList.stream()
					.filter(p -> p.getEdad() > 40)
					.peek(p -> System.out.println(p))
					.filter(p -> p.getCiudad().equals("Bogota"))
					.findFirst();
					
					if (oProfesor.isPresent()) {
						System.out.println(oProfesor.get());
					}
			
		}
		
		System.out.println("OPERACION count ------------");
		long profesoresBogota = profesorList.stream()
		.filter(p -> p.getCiudad().equals("Bogota"))
		.count();
		
		System.out.println(profesoresBogota);
		
		
		System.out.println("OPERACION sum ------------");
		Double sumatoria = profesorList.stream()
		.filter(p -> p.getSalario() >= 2000000.0)
		.peek(p -> System.out.println(p))
		.mapToDouble(p -> p.getSalario() * 0.11)
		.peek(r -> System.out.println("Retencion es de:" +  r))
		.sum();
		
		System.out.println("La retencion total es de:" + sumatoria);
		
		
		System.out.println("MAX Edad:");
		
		Optional<Profesor> op = profesorList.stream()
								.max(Comparator.comparing(Profesor::getEdad));
		
		if (op.isPresent()) {
			System.out.println("-----");
			System.out.println(op.get());
		}
		
		
		System.out.println("MIN Edad:");
		
		op = profesorList.stream()
								.min(Comparator.comparing(Profesor::getEdad));
		
		if (op.isPresent()) {
			System.out.println("-----");
			System.out.println(op.get());
		}
		
		
		
		System.out.println("AVERAGE---------");
		OptionalDouble od = profesorList.stream()
		.filter(p -> p.getCiudad().equals("Cali"))
		.peek(p -> System.out.println(p))
		.mapToDouble(p-> p.getSalario())
		.average();
		
		if (od.isPresent()) {
			System.out.println("Salario promedio en Cali: " + od.getAsDouble());
		}
		
		
		System.out.println("Sorted---------");
		profesorList.stream()
		.mapToLong(p -> p.getEdad())
		.sorted()
		.forEach(p -> System.out.println(p));
		
		
		System.out.println("Sorted con Comparator---------");
		profesorList.stream()
		.sorted(Comparator.comparing(Profesor::getNombre))
		.forEach(p -> System.out.println(p));
		
		
		System.out.println("Sorted con Comparator Orden inverso---------");
		profesorList.stream()
		.sorted(Comparator.comparing(Profesor::getNombre).reversed())
		.forEach(p -> System.out.println(p));
		
		
		System.out.println("Sorted con Comparator Multinivel---------");
		profesorList.stream()
		.sorted(Comparator.comparing(Profesor::getCiudad)
				.thenComparing(Profesor::getNombre)
				.thenComparing(Profesor::getApellido))
		.forEach(p -> System.out.println(p));
		
		
		System.out.println("Collect para almacenar el resultado---------");
		
		profesorList.forEach(p -> System.out.println(p));
		
		System.out.println("-----------------------");
		
		List<Profesor> nuevaListaProfesores = profesorList.stream()
		.sorted(Comparator.comparing(Profesor::getCiudad)
				.thenComparing(Profesor::getNombre)
				.thenComparing(Profesor::getApellido))
		.collect(Collectors.toList());
		
		nuevaListaProfesores.forEach(p -> System.out.println(p));
		
		
		System.out.println("Collect con operaciones matematicas---------");
		
		Double salarioPromedio = profesorList.stream()
		.collect(Collectors.averagingDouble(Profesor::getSalario));
		
		System.out.println("Salario promedio:" + salarioPromedio);
		
		
		System.out.println("Collect con Joing--------");
		
		String listaEncadenada = profesorList.stream()
		.map(p -> p.getNombre() + " " + p.getSalario().toString())
		.collect(Collectors.joining("| "));
		
		System.out.println(listaEncadenada);
		
		
		System.out.println("Collect con Grouping--------");
		Map<String, List<Profesor>> groupingMap = new HashMap<>();
		groupingMap = profesorList.stream()
		.collect(Collectors.groupingBy(Profesor::getCiudad));
		
		groupingMap.forEach((k, v) -> {
			System.out.println("Ciudad: " + k);
			v.forEach(p -> System.out.println(p));
		});
		
		
		System.out.println("Collect con Grouping and Count--------");
		Map<String, Long> gProfesoresCantidad = new HashMap<>();
		
		gProfesoresCantidad = profesorList.stream()
				.collect(Collectors. groupingBy(p -> p.getCiudad(), Collectors.counting()));
		
		gProfesoresCantidad.forEach((k, v) -> System.out.println("Ciudad: " + k + " Cantidad: " + v));
		
		
		System.out.println("Collect con Particion--------");
		Map<Boolean, List<Profesor>> grupoParticionado = new HashMap<>();
		
		grupoParticionado = profesorList.stream()
		.collect(Collectors.partitioningBy(p -> p.getSalario() <= 2000000.0));
		
		grupoParticionado.forEach((k, v) -> {
			System.out.println("Cumple: " + k);
			v.forEach(p -> System.out.println(p));
		});
		
		try {
			primeNumber();
		} catch (PrimeNumberException e)  {
			System.out.println("PNE " + e.getMessage());
		} catch (NotPrimeNumberException e) {
			System.out.println("NPNE " + e.getMessage());
		} catch (Exception e) {
			//error general
		}
	}
	
	public static <T> void print(T[] array) {
		
		for (T e : array) {
			System.out.println(e);
		}
		
	}
	
	
	
	public static List<Profesor> createPofesor() {
		List<Profesor> profesores = new ArrayList<>();
		
		Profesor profesor1 = new Profesor.Builder("Diego", "Lamprea")
				.edad(35L)
				.direccion("Calle 200")
				.salario(1000000.0)
				.ciudad("Bogota")
				.build();
		
		Profesor profesor2 = new Profesor.Builder("Carlos", "Chaparro")
				.edad(42L)
				.direccion("Calle 100")
				.ciudad("Cali")
				.salario(2000000.0)
				.build();
		
		Profesor profesor3 = new Profesor.Builder("Nelson", "Garcia")
				.edad(45L)
				.direccion("Calle 50")
				.ciudad("Bogota")
				.salario(2500000.0)
				.build();
		
		Profesor profesor4 = new Profesor.Builder("Camilo", "sanchez")
				.edad(39L)
				.direccion("Calle 20")
				.ciudad("Medellin")
				.salario(4000000.0)
				.build();
		
		profesores.add(profesor1);
		profesores.add(profesor2);
		profesores.add(profesor3);
		profesores.add(profesor4);
		
		return profesores;
		
		
		
	}
	
	
	
	
	
	private static void primeNumber() throws PrimeNumberException, NotPrimeNumberException {
		Double rdm = Math.floor(Math.random() * 10);
		System.out.println(rdm);
		int cont = 0;
		
		for (int i = 1; i<=rdm; i++) {
			
			if (rdm%i == 0) {
				cont ++;
			}
			
		}
		
		if (cont == 2) {
			throw new PrimeNumberException("El numero: " +rdm.intValue() + " es primo");
		} else {
			throw new NotPrimeNumberException("El numero: " + rdm.intValue() + " no es primo");
		}
		
		
	}
	
	
	
	
	

}
