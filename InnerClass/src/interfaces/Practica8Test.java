package interfaces;

public class Practica8Test {

	public static void main(String[] args) {

		Practica8 p8 = new Practica8();
		
		try {
			System.out.println(p8.login("japererz", "9999999"));

		} catch (UserNotFoundException e) {
			System.out.println("Usuario invalido");

		} catch (InvalidPasswordException e) {
			// TODO Auto-generated catch block
			System.out.println("Contraseña invalida, intente nuevamente");
		}

	}

}
