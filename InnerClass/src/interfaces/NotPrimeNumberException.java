package interfaces;

public class NotPrimeNumberException extends Exception {
	
	private static final long serialVersionUID = -7083866142833140228L;

	public NotPrimeNumberException() {

	}
	
	
	public NotPrimeNumberException(String message ) {
		super(message);
	}
	
	public NotPrimeNumberException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
